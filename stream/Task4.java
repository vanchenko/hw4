package stream;

import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Double> list = List.of(5.0, 1.0, 10.0, 8.0, 2.0);
        System.out.println("Исходная коллекция: " + list);
        System.out.println("Отсортированная по убыванию полученная коллекция: " + sortDoubleList(list));
    }

    public static List<Double> sortDoubleList(List<Double> list) {
        return list.stream()
                .sorted((a, b) -> Double.compare(b, a))
                .toList();
    }
}
