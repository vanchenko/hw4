package stream;

import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {
        int a = 1;
        int b = 100;
        System.out.println("Сумма чисел в последовательности от " + a + " до " + b + " равна " +
                getSumIntegerSequence(1, 100));
    }

    public static int getSumIntegerSequence(int startInclusive, int endInclusive){
        return IntStream
                .range(startInclusive, endInclusive + 1)
                .sum();
    }
}
