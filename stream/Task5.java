package stream;

import java.util.List;

public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qQq", "mmm");
        System.out.println("Исходная коллекция: " + list);
        System.out.println("Коллекция со строковыми элементами в верхнем регистре: " + toUpperCaseElementsList(list));
    }

    public static List<String> toUpperCaseElementsList(List<String> list){
        return list.stream()
                .map(el -> el.toUpperCase()).toList();

    }
}
