package stream;

import java.util.ArrayList;
import java.util.List;

public class Task6 {
    public static void main(String[] args) {
        List<Integer> list1 = List.of(1, 2, 3, 4, 5);
        List<Integer> list2 = List.of(6, 7, 8, 9, 10);
        List<Integer> list3 = List.of(11, 12, 13, 14, 15);

        List<List<Integer>> result = new ArrayList<>();
        result.add(list1);
        result.add(list2);
        result.add(list3);

        System.out.println("Исходная коллекция списков: " + result);
        System.out.println("Полученная коллекция значений : " + result);
    }

    public static List<Integer> combineLists(List<List<Integer>> lists){
        return lists.stream()
                .flatMap(list -> list.stream())
                .toList();
    }
}
