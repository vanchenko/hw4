package stream;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        System.out.println("Исходная коллекция: " + list);
        System.out.println("Результат умножения всех элементов в коллекции равен: " + multiplyElements(list));
    }

    public static int multiplyElements(List<Integer> list){
        Integer reduce = list.stream()
                .reduce(1, (a, b) -> a * b);
        return reduce;
    }
}
