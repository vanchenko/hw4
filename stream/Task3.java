package stream;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        System.out.println("Исходная коллекция: " + list);
        System.out.println("Количество непустых строк в коллекции: " + getCountNotEmptyString(list));
    }


    public static long getCountNotEmptyString(List<String> list){
        return list.stream()
                .filter(str -> !str.isEmpty())
                .count();
    }
}
